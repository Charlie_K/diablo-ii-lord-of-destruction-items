Done like this on purpose.

Reasoning: I do more with horadric cube these days and I am learning it's easier to keep things like these still sorted since they're also ingredients for a lot of things.


Note: I see what you mean, but it would still be sorted if you would rename them like this:
potion antidote
potion healing minor
potion healing light
potion healing regular
potion healing full
mana minor
etc and so on ;-)

Anyway, our system works for the most part, I just still have some caveats.
For example I wanted a runeword item the other day and started looking inside the GREY items folder (since it takes a grey socketed non-magical item to make the runewords)
However, I discovered that they are in the Gold items folder.. because once you create the runeword it becomes a magical item and the name in gold.. I do understand the logic
But still... I think I might prefer a separate Runeword item folder around, at the same level of gold etc and perhaps put blue - yellow - gold in a 'magic item' folder as subfolder or such
Also green item.. still not sure whether to call it green item or SET ITEM
Or just re-colour the folder but rename it set item... of course we would each have to colour our folders individually UNLESS we can make use of those stupid desktop.ini thumbs.db files
to actually change the folder icons to reflect colours (I am sure we can download folder items somewhere)